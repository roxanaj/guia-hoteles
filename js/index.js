$(function(){
            $("[data-toggle='tooltip']").tooltip();
            $("[data-toggle='popover']").popover();
            $('.carousel').carousel({
                interval: 3000
            });
            $('#contacto').on('show.bs.modal',function(e){
                console.log('Mostrando el modal');
                $('#contactoBtn').removeClass('btn-outline-primary');
                $('#contactoBtn').addClass('btn-outline-success');
                $('#contactoBtn').prop('disabled',true);
            });
            $('#contacto').on('shown.bs.modal',function(e){
                console.log('Se mostró el modal');
            });
            $('#contacto').on('hide.bs.modal',function(e){
                console.log('Se oculta el modal');
            });
            $('#contacto').on('hidden.bs.modal',function(e){
                console.log('Se ocultó el modal');
                $('#contactoBtn').prop('disabled',false);
            });
});